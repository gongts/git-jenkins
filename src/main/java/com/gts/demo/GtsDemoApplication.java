package com.gts.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author gts
 * @version 1.0
 * @description
 * @date 30/8/2023 下午4:50
 * @since 1.0
 * <p>
 * Copyright (c) 2023, huitu All Rights Reserved.
 */
@SpringBootApplication(scanBasePackages = {"com.gts.demo"})
@MapperScan("com.gts.demo.mapper")
public class GtsDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(GtsDemoApplication.class, args);
    }
}