FROM openjdk:11.0
ADD gts-jenkins.jar /gts-jenkins.jar
EXPOSE 8081
CMD java -Djava.security.egd=file:/dev/./urandom -jar /gts-jenkins.jar